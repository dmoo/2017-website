var webpack = require('webpack');
var path = require('path');

var appName = 'app';
var plugins = [];

var outputFile = appName + '.js';

var config = {
  entry: './js/index.js',
  devtool: 'cheap-module-source-map',
  output: {
    path: __dirname + '/dist',
    publicPath: '/',
    filename: outputFile,
  },
  module: {
    rules: [{
        test: /(\.jsx|\.js)$/,
          loader: 'babel-loader',
          exclude: /(node_modules|bower_components)/
      },
      {
        test: /\.scss$/,
        loaders: ["style-loader", "css-loader", "sass-loader"]
      },
      { test: /\.svg$/,               loader: 'url-loader?limit=65000&mimetype=image/svg+xml&name=public/fonts/[name].[ext]' },
      { test: /\.woff$/,              loader: 'url-loader?limit=65000&mimetype=application/font-woff&name=public/fonts/[name].[ext]' },
      { test: /\.woff2$/,             loader: 'url-loader?limit=65000&mimetype=application/font-woff2&name=public/fonts/[name].[ext]' },
      { test: /\.[ot]tf$/,            loader: 'url-loader?limit=65000&mimetype=application/octet-stream&name=public/fonts/[name].[ext]' },
      { test: /\.eot$/,               loader: 'url-loader?limit=65000&mimetype=application/vnd.ms-fontobject&name=public/fonts/[name].[ext]' },
      { test: /\.(jpe?g|png|gif)$/i,  loader: 'file-loader?name=public/images/[name].[ext]' },
  ]},
  plugins: plugins
};

module.exports = config;