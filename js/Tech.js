import React from 'react';
import * as Components from './components';

var Tech = React.createClass({
  render: function () {
    if (this.props.onRender) {
      this.props.onRender();
    }
    return (
      <div className="max-width--lg">
        <Components.TechHeader/>
        <Components.Tech/>
        <Components.SideProjects/>
      </div>
    );
  },
  propTypes: {
    onRender: React.PropTypes.func
  }
});

export default Tech;