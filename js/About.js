import React from 'react';
import * as Components from './components';

var About = React.createClass({
  render: function () {
    if (this.props.onRender) {
      this.props.onRender();
    }
    return (
      <div className="max-width--lg">
        <Components.AboutHeader/>
        <Components.About/>
      </div>
    );
  },
  propTypes: {
    onRender: React.PropTypes.func
  }
});

export default About;