import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, hashHistory, IndexRoute } from 'react-router';
import App from './App';
import CV from './CV';
import Contact from './Contact';
import About from './About';
import Tech from './Tech';

require('../public/fonts/open_sans.scss');
require('../node_modules/reflex-grid/scss/reflex.scss');
require('../scss/main.scss');

require('file-loader?name=[name].[ext]!../index.html');

window.onload = () => {

	function scrollToMenu() {
	    setTimeout(() => {
	      const element = document.getElementById('menu');
	      if (element) element.scrollIntoView( { behavior: 'smooth' } );
	    }, 0);
	}

	ReactDOM.render((
	  <Router history={hashHistory}
	  		  onUpdate={scrollToMenu}>
	    <Route path="/" component={App}>
	    	<IndexRoute component={CV} />
	      <Route path="cv" component={CV}/>
	      <Route path="tech" component={Tech}/>
	      <Route path="contact" component={Contact}/>
	      <Route path="about" component={About}/>
	    </Route>
	  </Router>
	), document.querySelector('#container'))

};