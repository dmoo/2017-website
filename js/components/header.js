import React, { Component } from 'react';
import { Link } from 'react-router';
import * as Components from '../components';

export default class Header extends React.Component {
  render() {
    return (
    	<header>
		   <section className="grid">

	            <div className="grid__col-md-4 hard face grid__col--bg">&nbsp;</div>

		         <div className="grid__col-md-8 hard banner">

		            <div className="grid">
		               <div className="grid__col-md-8 banner--block banner--dark-1">
		               	 <h1 className="flush soft--ends" id="name">DAVID MOORHEAD</h1>
		               </div>
		               <div className="grid__col-md-4 banner--block banner--dark-2 grid--justify-center text--center hide--small">
		               	 <Components.StackOverflow/>
		               </div>
		            </div>
		            <div className="grid">
		               <div className="grid__col-md-8 banner--block banner--dark-3">
	                     <h2 className="flush soft--ends">WEB DEVELOPER</h2>
		               </div>
		               <div className="grid__col-md-4 hard banner--block banner--dark-4 grid--justify-center text--center hide--small">
						<span>
							<a href="https://github.com/dmoojunk"><i className="fa fa-github fa-2x"></i></a>
							&nbsp;&nbsp;&nbsp;
							<a href="https://bitbucket.org/dmoo/"><i className="fa fa-bitbucket fa-2x"></i></a>
							&nbsp;&nbsp;&nbsp;
							<a href="http://stackoverflow.com/users/1237742/dmoo"><i className="fa fa-stack-overflow fa-2x"></i></a>
							&nbsp;&nbsp;&nbsp;
							<a href="http://codepen.io/dmoojunk/"><i className="fa fa-codepen fa-2x"></i></a>
						</span>
		               </div>
		            </div>


		            <ul className="grid text--center" id="menu">
		               <li className="grid__col-3 grid__col-sm-3 hard banner--block banner--green grid--justify-center">
		                  <Link to={`/cv`}><i className="fa fa-graduation-cap fa-2x"></i><p className="flush soft-half--top">CV</p></Link>
		               </li>
		               <li className="grid__col-3 grid__col-sm-3 hard banner--block banner--blue grid--justify-center">
		                  <Link to={`/tech`}><i className="fa fa-code fa-2x"></i><p className="flush soft-half--top">TECH</p></Link>
		               </li>
		               <li className="grid__col-3 grid__col-sm-3 hard banner--block banner--purple grid--justify-center">
		                  <Link to={`/contact`}><i className="fa fa-map-marker fa-2x"></i><p className="flush soft-half--top">CONTACT</p></Link>
		               </li>
		               <li className="grid__col-3 grid__col-sm-3 hard banner--block banner--orange grid--justify-center">
		                  <Link to={`/about`}><i className="fa fa-info fa-2x"></i><p className="flush soft-half--top">ABOUT</p></Link>
		               </li>
		            </ul>


		         </div>
		   </section>
		</header>
    );
  }
}