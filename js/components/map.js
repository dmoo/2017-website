import React, { Component } from 'react';
import { withGoogleMap, GoogleMap } from 'react-google-maps';

const MyMap = withGoogleMap(props => (
  <GoogleMap
  	options={{ disableDefaultUI: true, draggable: false, zoomControl: false, scrollwheel: false, disableDoubleClickZoom: true }}
    ref={props.onMapLoad}
    defaultZoom={13}
    defaultCenter={{ lat: 54.5883188, lng: -5.9144744 }}
    onClick={props.onMapClick}
  >
  </GoogleMap>
));

export default class Map extends React.Component {

	render() {
    return (
    	<div className="grid">
    		<div className="grid__col-auto hard">
			  <MyMap
			    containerElement={
			      <div style={{ height: `20em` }} />
			    }
			    mapElement={
			      <div style={{ height: `20em` }} />
			    }
			  />
			</div>
		</div>
		);
  }
}