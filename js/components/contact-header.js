import React, { Component } from 'react';


export default class ContactHeader extends React.Component {
  render() {
    return (
      <section className="header--purple grid" id="contact">
      	<div className="grid__col-12 grid__cell--padding-lg">
      		<h1 className="flush--bottom">Contact</h1>
      	</div>
      </section>
    );
  }
}