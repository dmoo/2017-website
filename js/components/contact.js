import React, { Component } from 'react';

export default class Contact extends React.Component {
  render() {
    return (
      <section className="grid">
      	<div className="grid__col-md-4 grid__cell--padding-lg bg--gray">
      		<p className="flush--bottom">Lets have a chat</p>
      	</div>
        <div className="grid__col-md-8 grid__cell--padding-lg bg--white">
          <ul className="bare flush--bottom">
            <li className="grid"><label className="grid__col-auto hard"><b>Name:</b></label><label className="grid__col-auto hard text--right">David Moorhead</label></li>
            <li className="grid"><label className="grid__col-auto hard"><b>Address:</b></label><label className="grid__col-auto hard text--right">Ravenhill, Belfast</label></li>
            <li className="grid"><label className="grid__col-auto hard"><b>Phone:</b></label><label className="grid__col-auto hard text--right"><a href="tel:07780002390">077 8000 2390</a></label></li>
            <li className="grid"><label className="grid__col-auto hard"><b>Email:</b></label><label className="grid__col-auto hard text--right"><a href="mailto:david.moorhead1@gmail.com?Subject=Hello" target="_top">david.moorhead1@gmail.com</a></label></li>
          </ul>
        </div>
      </section>
    );
  }
}