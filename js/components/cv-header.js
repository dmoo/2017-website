import React, { Component } from 'react';

export default class CVHeader extends React.Component {
  render() {
    return (
      <section className="header--green grid" id="cv">
      	<div className="grid__col-12 grid__cell--padding-lg">
      		<h1>Curriculum Vitae</h1>
      		<p>A full stack developer specialising in the front end. I have experience leading technical teams in large deliveries, working to deadlines and delivering best practice solutions. Professionally driven with the ability to work under pressure and able to analytically solve problems, keep abreast of current trends and quickly learn new skills. An effective team member with excellent technical and communicative skills, over 8 years of experience in a variety of development roles and who can bring an enthusiastic approach to technical delivery.</p>
      	</div>
      </section>
    );
  }
}