import React, { Component } from 'react';

export default class Eastpak extends React.Component {
  render() {
    return (
    	<section className="grid">
    		<div className="grid__col-md-4 grid__cell--padding-lg bg--gray grid--order-1 grid--order-0-md">
    			<h3>Eastpak showcase</h3>
                <ul className="flush--bottom">
                    <li>AngularJS touch shopping application</li>
                    <li>CSS3 Animations</li>
                    <li>Prototype Samsung 65 inch touchscreen tvs</li>
                    <li>Integration with <a href="https://www.shopify.co.uk/">shopify</a> and backend eastpak order processing systems</li>
                    <li>Lead the technical delivery of a team of three developers</li>
                    <li>Delivered in two months</li>
                    <li>Apprentice challenge style shop installation including frantic calls to Italian customs who had impounded our fancy tvs and hunting down an all night Tesco at 2AM to buy laptops</li>
                </ul>
    		</div>
    		<div className="grid__col-md-8 hard grid--order-0 grid--order-1-md">
    			<div className='embed-container' style={{ height: '100%', 'min-height': '400px'}}><iframe src='https://player.vimeo.com/video/123772763' frameBorder='0' allowFullScreen></iframe></div>
    		</div>
    	</section>
	);
  }
}