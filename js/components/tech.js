import React, { Component } from 'react';


export default class Tech extends React.Component {
  render() {
    return (
      <section className="grid">
      	<div className="grid__col-sm-6 grid__col-md-3 grid__cell--padding-lg bg--white grid--order-0">
          <h3>IDEs</h3>
          <ul className="flush--bottom bare">
            <li>Sublime Text</li>
            <li>Atom</li>
            <li>IntelliJ</li>
            <li>Visual Studio</li>
          </ul>
        </div>
        <div className="grid__col-sm-6 grid__col-md-3 grid__cell--padding-lg bg--gray grid--order-1">
          <h3>Build Tools</h3>
          <ul className="flush--bottom bare">
            <li>Webpack</li>
            <li>Babel</li>
            <li>Gulp</li>
            <li>Grunt</li>
            <li>NPM</li>
            <li>Bower</li>
          </ul>
        </div>
        <div className="grid__col-sm-6 grid__col-md-3 grid__cell--padding-lg bg--gray grid--order-3-xs grid--order-2-sm grid--order-3-md">
          <h3>Frameworks & Libraries</h3>
          <ul className="flush--bottom bare">
            <li>React & Redux & Immutable</li>
            <li>Angular & Ionic</li>
            <li>Knockout</li>
            <li>Polymer</li>
            <li>jQuery</li>
            <li>SASS</li>
            <li>Moment & Lodash & Express</li>
            <li>Bootstrap</li>
          </ul>
        </div>
        <div className="grid__col-sm-6 grid__col-md-3 grid__cell--padding-lg bg--white grid--order-2-xs grid--order-3-sm grid--order-2-md">
          <h3>Testing Tools</h3>
          <ul className="flush--bottom bare">
            <li>Mocha & Karma & Chai & Sinon</li>
            <li>Jest</li>
            <li>Phantom JS</li>
            <li>JS DOM</li>
            <li>Jasmine</li>
            <li>Browserstack</li>
            <li>Selenium</li>
          </ul>
        </div>
      </section>
    );
  }
}