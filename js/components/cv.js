import React, { Component } from 'react';

export default class CV extends React.Component {
  render() {
    return (
    	<section className="grid">
    		<div className="grid__col-md-8 grid__cell--padding-lg bg--white">
    			<h3>Professional Skills</h3>
                <ul className="flush--bottom">
                    <li>A seasoned agile developer</li>
                    <li>Accessibility aware, cross browser and multi device experienced </li>
                    <li>Comfortable working with backend developers to design and implement the API/controller tier</li>
                    <li>Comfortable discussing designs and UX flow with creatives</li>
                    <li>Lead agile meetings and mentor junior staff</li>
                    <li>Express myself to a wide range of audiences at the appropriate level</li>
                    <li>Spent years on client sites around the UK implementing solutions</li>
                    <li>Big name clients such as  <a href="https://www.aldi.co.uk/">Aldi</a>, <a href="www.youandb.co.uk">B Bank</a> and <a href="www.eastpak.com">Eastpak</a></li>
                </ul>
    		</div>
    		<div className="grid__col-md-4 grid__cell--padding-lg bg--gray">
    			<h3>Personal Info</h3>
    			<ul className="bare">
    				<li className="grid"><label className="grid__col-auto hard"><b>Name:</b></label><label className="grid__col-auto hard text--right">David Moorhead</label></li>
    				<li className="grid"><label className="grid__col-auto hard"><b>Address:</b></label><label className="grid__col-auto hard text--right">Ravenhill, Belfast</label></li>
    				<li className="grid"><label className="grid__col-auto hard"><b>Phone:</b></label><label className="grid__col-auto hard text--right"><a href="tel:07780002390">077 8000 2390</a></label></li>
    				<li className="grid"><label className="grid__col-auto hard"><b>Email:</b></label><label className="grid__col-auto hard text--right"><a href="mailto:david.moorhead1@gmail.com?Subject=Hello" target="_top">david.moorhead1@gmail.com</a></label></li>
    			</ul>
    		</div>
    	</section>
		);
  }
}