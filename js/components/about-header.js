import React, { Component } from 'react';


export default class AboutHeader extends React.Component {
  render() {
    return (
      <section className="header--orange grid" id="about">
      	<div className="grid__col-12 grid__cell--padding-lg">
      		<h1>About this site</h1>
      		<p>I end up refreshing my site every once in a while. I am really enjoying React and thought this would be a good outlet for me to play around. <br/>
          Its is clearly over-engineered for a static website, but that is the point. I invite you to go have a look at the code behind this website as I have criminally little publically available on the interwebs.</p>
          <p><a href="https://bitbucket.org/dmoo/2017-website/src" style={{color: 'white'}}><i className="fa fa-bitbucket"></i> 2017-website on Bitbucket</a></p>
      	</div>
      </section>
    );
  }
}