import React, { Component } from 'react';

export default class SideProjects extends React.Component {
  render() {
    return (
    	<section className="grid">
    		<div className="grid__col-md-6 grid__cell--padding-lg bg--white">
    			<h3>Work Side Projects</h3>
                <p>I love a good mashup and am actively involved at every chance I get in work.</p>
                <ul className="flush--bottom">
                <li>Recently it has been working on a POC for an iPad outside every office in CME syncing with exchange and allowing hot booking of rooms.</li>
                <li>Experimenting with the various app container projects like cordova and ionic, seeing how easy it would be to drop an existing application inside a wrapper.</li>
                <li>For Deloitte visitors I created a D3 interactive display that allowed viewing of employees profiles, drilling in for details and an up to date location to see where they were working in the world on a map.</li>
                <li>Wrote a frontend in polymer to show off a friends JBoss Drools demonstation at the Red Hat Summit used live by the audience.</li>
                </ul>
    		</div>
    		<div className="grid__col-md-6 grid__cell--padding-lg bg--gray">
    			<h3>Open Source</h3>
                <p>
                    I became interested in Reflex grid when designing the enterprise UI framework for CME. It utilizes flexbox in a familiar and accesible way.<br/>
                    I ported it from LESS to SASS and have delivered a few features and remained active in the project along the way.
                </p>
                <a className="embedly-card" href="https://github.com/leejordan/reflex">leejordan/reflex</a>
                
    		</div>
    	</section>
	);
  }
}