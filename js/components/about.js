import React, { Component } from 'react';


export default class About extends React.Component {
  render() {
    return (
      <section className="grid">
      	<div className="grid__col-md-4 grid__cell--padding-lg bg--white">
          <h3>Tech</h3>
          <ul className="flush--bottom">
            <li>Webpack 2</li>
            <li>NPM</li>
            <li>React/React Router and Babel</li>
            <li>Font Awesome</li>
            <li>SASS</li>
            <li>ITCSS</li>
            <li><a href="http://www.csswizardry.com">Inuit CSS</a></li>
          </ul>
        </div>
        <div className="grid__col-md-8 grid__cell--padding-lg bg--gray">
          <h3>Notes</h3>
          <p>My favourite part of this site is the CSS. I attended a training course with Harry Roberts and be completely sold me on BEM naming and ITCSS. I am now a big advocate for his patterns and practices, I roll them into every project I start now.</p>
          <p>To keep up the checkboard grey/white throughout the breakpoints I got to experiment with the flex order property.</p>
          <p>Webpack 2 was an real pain with the the introduction of native tree shaking. Many git bug reports later, I resolved to having everything babel '^6' in package.json to get the bleeding edge. </p>
        </div>
      </section>
    );
  }
}