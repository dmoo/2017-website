import React, { Component } from 'react';
const face = require("../../public/images/face.jpg");

export default class Timeline extends React.Component {
  render() {
    return (
      <div className="grid bg--white">
        <div className="grid__col-12 grid__cell--padding-lg">
          <h3>What I have been up to...</h3>
          <ul className="timeline">
            <li>
              <div className="timeline--item">
                <div className="timeline--flag-wrapper">
                  <span className="timeline--flag">CME Group</span>
                  <span className="timeline--time h5">FEB 2016 - Present</span>
                </div>
                <div className="timeline--desc h5"><b>Senior Web Developer</b> - Producing the enterprise UI component framework utilising React, Webpack, ES6 and SASS while dabbling in Java</div>
              </div>
            </li>
            <li>
              <div className="timeline--item">
                <div className="timeline--flag-wrapper">
                  <span className="timeline--flag">Deloitte Digital</span>
                  <span className="timeline--time h5">AUGUST 2011 - FEB 2016</span>
                </div>
                <div className="timeline--desc h5"><b>Belfast Web Lead</b> for one of the big four. A dynamic role producing production HTML, JS, CSS,  Knockout, Angular, React, C# MVC4 & Python for a wide variety of web applications.</div>
              </div>
            </li>
            <li>
              <div className="timeline--item">
                <div className="timeline--flag-wrapper">
                  <span className="timeline--flag">Singularity</span>
                  <span className="timeline--time h5">JULY 2008 - AUG 2011</span>
                </div>
                <div className="timeline--desc h5"><b>Consultant</b> for a Business Process Management company. Using ASP.net, C#, MS SQL and their own Singularity Process Platform (SPP) I travelled the UK implementing custom BPM solutions onsite.</div>
              </div>
            </li>
            <li>
              <div className="timeline--item">
                <div className="timeline--flag-wrapper">
                  <span className="timeline--flag">Mencap</span>
                  <span className="timeline--time h5">JUNE 2008</span>
                </div>
                <div className="timeline--desc h5"><b>Contractor</b> for a charity. I created a relational database to help query and report on Mencaps data.</div>
              </div>
            </li>
            <li>
              <div className="timeline--item">
                <div className="timeline--flag-wrapper">
                  <span className="timeline--flag">Arts Council of NI</span>
                  <span className="timeline--time h5">JUNE 2006 - AUG 2007</span>
                </div>
                <div className="timeline--desc h5"><b>Student Placement</b> at the Arts Council of Northern Ireland. I took on a dual role, general support for the staff and implementing a solution in classic ASP/SQL to capture data from recently funded organisations</div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}