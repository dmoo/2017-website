import React, { Component } from 'react';


export default class TechHeader extends React.Component {
  render() {
    return (
      <section className="header--blue grid" id="tech">
      	<div className="grid__col-12 grid__cell--padding-lg">
      		<h1>Technologies</h1>
      		<p>I love code. I love learning anything new. I have had particular fun this year learning ES6 and flexbox patterns.</p>
      	</div>
      </section>
    );
  }
}