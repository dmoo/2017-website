import React from 'react';
import * as Components from './components';

var Contact = React.createClass({
  render: function () {
    if (this.props.onRender) {
      this.props.onRender();
    }
    return (
      <div className="max-width--lg">
        <Components.ContactHeader/>
        <Components.Contact/>
        <Components.Map/>
      </div>
    );
  },
  propTypes: {
    onRender: React.PropTypes.func
  }
});

export default Contact;