import React from 'react';
import * as Components from './components';

var CV = React.createClass({
  render: function () {
    if (this.props.onRender) {
      this.props.onRender();
    }
    return (
      <div className="max-width--lg">
        <Components.CVHeader/>
        <Components.CV/>
        <Components.Eastpak/>
        <Components.Timeline/>
      </div>
    );
  },
  propTypes: {
    onRender: React.PropTypes.func
  }
});

export default CV;