import React from 'react';
import * as Components from './components';

var App = React.createClass({
  render: function () {
    if (this.props.onRender) {
      this.props.onRender();
    }
    return (
      <div className="max-width--lg">
        <Components.Header/>
        {this.props.children}
      </div>
    );
  },
  propTypes: {
    onRender: React.PropTypes.func
  }
});

export default App;